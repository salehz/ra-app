import 'package:flutter/material.dart';
import 'package:ra_app/pages/daily_dashboard.dart';
import 'package:ra_app/theme/theme.dart';

main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: DailyDashbordFA(),
    darkTheme: dark,
    themeMode: ThemeMode.dark,
  ));
}
