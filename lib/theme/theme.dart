import 'package:flutter/material.dart';

class RAcolors {
  static const Color darkbackground = Color.fromRGBO(16, 16, 16, 1);
  static const Color brown_grey = Color.fromRGBO(158, 158, 158, 1);
  static const Color title = Color.fromRGBO(33, 33, 33, 1);
  static const Color card = Color.fromRGBO(255, 255, 255, 1);
  static const Color very_light_pink = Color.fromRGBO(196, 196, 196, 1);
  static const Color greenblue = Color.fromRGBO(38, 191, 144, 1);
  static const Color cherry_red = Color.fromRGBO(229, 28, 35, 1);
}

class RAtextStyle {
  static const textStyle = TextStyle(
      fontFamily: 'IRANYekan', fontSize: 13, color: RAcolors.very_light_pink);
  static const textStyle2 = TextStyle(
      fontFamily: 'IRANYekan', fontSize: 14, color: RAcolors.very_light_pink , height: 1.5);
  static const textStyle3 =
      TextStyle(fontFamily: 'IRANYekan', fontSize: 14, color: RAcolors.card);
  static const textStyle4 = TextStyle(
      fontFamily: 'IRANYekan',
      fontSize: 12,
      fontWeight: FontWeight.w300,
      color: RAcolors.card);
  static const textStyle5 = TextStyle(
      fontFamily: 'IRANYekan', fontSize: 12, color: RAcolors.brown_grey);
  static const textStyle6 = TextStyle(
      fontFamily: 'IRANYekan', fontSize: 14, color: RAcolors.greenblue);
  static const textStyle7 = TextStyle(
      fontFamily: 'IRANYekan', fontSize: 12, color: RAcolors.very_light_pink);
  static const textStyle8 = TextStyle(
      fontFamily: 'IRANYekan', fontSize: 16, color: RAcolors.very_light_pink);
}

ThemeData dark = ThemeData(
    brightness: Brightness.dark,
    primaryColor: RAcolors.darkbackground,
    textTheme: TextTheme(
        bodyText1: RAtextStyle.textStyle2,
        subtitle1: RAtextStyle.textStyle8,
        headline1: RAtextStyle.textStyle,
        headline2: RAtextStyle.textStyle3,
        headline3: RAtextStyle.textStyle4,
        headline4: RAtextStyle.textStyle5,
        headline5: RAtextStyle.textStyle6,
        headline6: RAtextStyle.textStyle7),
    backgroundColor: RAcolors.darkbackground,
    iconTheme: IconThemeData(
      color: RAcolors.very_light_pink
    ) ,
    colorScheme: ColorScheme.dark(primary: RAcolors.title , secondary: RAcolors.greenblue ) , 
    
    );
