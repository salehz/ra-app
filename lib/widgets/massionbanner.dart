import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class RAMassionBanner extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<BoxShadow> raBoxShadows = [
      BoxShadow(
        color: Theme.of(context).colorScheme.secondary.withAlpha(60),
        offset: Offset(-3, 0),
        blurRadius: 6.0,
        spreadRadius: 0.0,
      ),
      BoxShadow(
        color: Theme.of(context).colorScheme.secondary.withAlpha(60),
        offset: Offset(0, 3),
        blurRadius: 15.0,
        spreadRadius: 0.0,
      ),
    ];
    return Container(
      width: 343,
      height: 120,
      alignment: Alignment.center,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 24, horizontal: 16),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Expanded(
                flex: 1,
                child: Container(
                  height: 56,
                  width: 56,
                  child: Icon(Icons.wb_sunny ,color: Theme.of(context).colorScheme.secondary,),
                  decoration: BoxDecoration(
                    color: Theme.of(context).colorScheme.primary.withOpacity(0.85),
                      border: Border.all(
                          color: Theme.of(context).colorScheme.secondary),
                      shape: BoxShape.circle,
                      boxShadow: raBoxShadows),
                )),
            SizedBox(
              width: 16,
            ),
            Expanded(
              flex: 4,
              child: Text(
                "ماموریت های امروز شما لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم گرافیک است",
                softWrap: true,
                overflow: TextOverflow.fade,
                maxLines: 3,
                textAlign: TextAlign.right,
                style: Theme.of(context).textTheme.bodyText1,
              ),
            )
          ],
        ),
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(16),
        color: Theme.of(context).colorScheme.primary,
      ),
    );
  }
}
