import 'package:flutter/material.dart';

class RABottomNavigation extends StatelessWidget {
  const RABottomNavigation({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
      List<BoxShadow> raBoxShadows = [
      BoxShadow(
        color: Theme.of(context).colorScheme.secondary.withAlpha(60),
        offset: Offset(-3, 0),
        blurRadius: 6.0,
        spreadRadius: 0.0,
      ),
      BoxShadow(
        color: Theme.of(context).colorScheme.secondary.withAlpha(60),
        offset: Offset(0, 3),
        blurRadius: 15.0,
        spreadRadius: 0.0,
      ),
    ];

    return Align(
      alignment: Alignment.bottomCenter,
      child: Container(
        height: 100,
        padding: EdgeInsets.only(bottom: 16),
        alignment: Alignment.bottomCenter,
        decoration: BoxDecoration(
            color: Theme.of(context).backgroundColor,
            gradient: LinearGradient(
                begin: FractionalOffset.center,
                end: FractionalOffset.topCenter,
                colors: [
                  Theme.of(context).backgroundColor,
                  Theme.of(context).backgroundColor.withOpacity(0.0)
                ])),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            ///Menu
            Container(
              height: 48,
              width: 48,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Theme.of(context).colorScheme.primary),
              child: InkWell(
                child: Icon(Icons.menu),
              ),
            ),

            ///Oval
            Container(
              height: 48,
              width: 48,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Theme.of(context).colorScheme.primary),
              child: InkWell(
                child: Icon(Icons.trending_up),
              ),
            ),

            ///Check
            Container(
              height: 52,
              width: 52,
              decoration: BoxDecoration(
                boxShadow: raBoxShadows,
                  shape: BoxShape.circle,
                  color: Theme.of(context).colorScheme.secondary),
              child: InkWell(
                child: Icon(Icons.check , size: 36,),
              ),
            ),

            ///Point
            Container(
              height: 48,
              width: 48,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Theme.of(context).colorScheme.primary),
              child: InkWell(
                child: Icon(Icons.place),
              ),
            ),

            ///Today
            Container(
              height: 48,
              width: 48,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  boxShadow: raBoxShadows,
                  border: Border.all(
                      color: Theme.of(context).colorScheme.secondary),
                  color: Theme.of(context).colorScheme.primary),
              child: InkWell(
                child: Icon(
                  Icons.today,
                  color: Theme.of(context).colorScheme.secondary,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
