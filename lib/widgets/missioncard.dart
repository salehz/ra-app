import 'package:flutter/material.dart';

class RAMissionCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<BoxShadow> raBoxShadows = [
      BoxShadow(
        color: Theme.of(context).colorScheme.secondary.withAlpha(60),
        offset: Offset(-3, 0),
        blurRadius: 6.0,
        spreadRadius: 0.0,
      ),
      BoxShadow(
        color: Theme.of(context).colorScheme.secondary.withAlpha(60),
        offset: Offset(0, 3),
        blurRadius: 15.0,
        spreadRadius: 0.0,
      ),
    ];

    return Column(
      children: <Widget>[
        ///first mission
        Opacity(
          opacity: 0.4,
          child: Container(
            height: 68,
            alignment: Alignment.centerRight,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(16),
              color: Theme.of(context).colorScheme.primary,
            ),
            margin: EdgeInsets.only(bottom: 16),
            child: ListTile(
              title: Text(
                "ثبت نام",
                style: Theme.of(context)
                    .textTheme
                    .bodyText1
                    .copyWith(decoration: TextDecoration.lineThrough),
                maxLines: 1,
                overflow: TextOverflow.fade,
              ),
              leading: Container(
                width: 36,
                height: 36,
                decoration: BoxDecoration(
                    color: Colors.transparent,
                    shape: BoxShape.circle,
                    border:
                        Border.all(color: Theme.of(context).iconTheme.color)),
                child:
                    Icon(Icons.check, color: Theme.of(context).iconTheme.color),
              ),
            ),
          ),
        ),

        ///ّsecond mission
        Container(
          height: 68,
          alignment: Alignment.centerRight,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(16),
              boxShadow: raBoxShadows,
              color: Theme.of(context).colorScheme.primary,
              border: Border.all(
                  color: Theme.of(context).colorScheme.secondary, width: 0.5)),
          margin: EdgeInsets.only(
            bottom: 16,
          ),
          child: ListTile(
            title: Text(
              "با نقشه ها آشنا شو",
              style: Theme.of(context)
                  .textTheme
                  .headline5
                  .copyWith(shadows: raBoxShadows),
              maxLines: 1,
              overflow: TextOverflow.fade,
            ),
            leading: Container(
              width: 36,
              height: 36,
              decoration: BoxDecoration(
                color: Colors.transparent,
                boxShadow: [
                  BoxShadow(
                    color:
                        Theme.of(context).colorScheme.secondary.withAlpha(60),
                    offset: Offset(0, 3),
                    blurRadius: 15.0,
                    spreadRadius: 0.0,
                  ),
                ],
                border:
                    Border.all(color: Theme.of(context).colorScheme.secondary),
                shape: BoxShape.circle,
              ),
              child: Icon(Icons.map,
                  color: Theme.of(context).colorScheme.secondary),
            ),
            subtitle: Text(
              "11 ساعت 24 دقیقه تا اتمام مهلت",
              style: Theme.of(context)
                  .textTheme
                  .headline6
                  .copyWith(decoration: TextDecoration.none),
              maxLines: 1,
              overflow: TextOverflow.fade,
              softWrap: true,
            ),
            trailing: Container(
              width: 21,
              height: 21,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                color: Colors.transparent,
                boxShadow: [
                  BoxShadow(
                    color:
                        Theme.of(context).colorScheme.secondary.withAlpha(60),
                    offset: Offset(0, 0),
                    blurRadius: 6.0,
                    spreadRadius: 0.0,
                  ),
                ],
                border:
                    Border.all(color: Theme.of(context).colorScheme.secondary),
                shape: BoxShape.circle,
              ),
              child: Icon(Icons.arrow_left,
                  size: 20, color: Theme.of(context).colorScheme.secondary),
            ),
          ),
        ),

        ///third mission
        Opacity(
          opacity: 0.4,
          child: Container(
            height: 68,
            alignment: Alignment.centerRight,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(16),
              color: Theme.of(context).colorScheme.primary,
            ),
            margin: EdgeInsets.only(
              bottom: 16,
            ),
            child: ListTile(
              title: Text(
                "هویت مخفی خودت را بساز",
                style: Theme.of(context)
                    .textTheme
                    .bodyText1
                    .copyWith(decoration: TextDecoration.none),
                maxLines: 1,
                overflow: TextOverflow.fade,
              ),
              leading: Container(
                width: 36,
                height: 36,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(color: Theme.of(context).iconTheme.color),
                ),
                child:
                    Icon(Icons.https, color: Theme.of(context).iconTheme.color),
              ),
            ),
          ),
        ),

        ///fourth mission
        Opacity(
          opacity: 0.4,
          child: Container(
            height: 68,
            alignment: Alignment.centerRight,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(16),
              color: Theme.of(context).colorScheme.primary,
            ),
            margin: EdgeInsets.only(bottom: 16),
            child: ListTile(
              title: Text(
                "******",
                style: Theme.of(context)
                    .textTheme
                    .bodyText1
                    .copyWith(decoration: TextDecoration.lineThrough),
                maxLines: 1,
                overflow: TextOverflow.fade,
              ),
              leading: Container(
                width: 36,
                height: 36,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border:
                        Border.all(color: Theme.of(context).iconTheme.color)),
                child:
                    Icon(Icons.https, color: Theme.of(context).iconTheme.color),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
