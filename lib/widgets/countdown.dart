import 'package:flutter/material.dart';

class ARCountDown extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Column(
              children: <Widget>[
                Text(
                  "24",
                  textAlign: TextAlign.center,
                  style: Theme.of(context)
                      .textTheme
                      .headline2
                      .copyWith(fontSize: 28),
                ),
                Text(
                  "ثانیه",
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline4,
                ),
              ],
            ),
            Text(":",
                style: Theme.of(context)
                    .textTheme
                    .headline2
                    .copyWith(fontSize: 28)),
            Column(
              children: <Widget>[
                Text(
                  "32",
                  textAlign: TextAlign.center,
                  style: Theme.of(context)
                      .textTheme
                      .headline2
                      .copyWith(fontSize: 28),
                ),
                Text(
                  "دقیقه",
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline4,
                ),
              ],
            ),
            Text(":",
                style: Theme.of(context)
                    .textTheme
                    .headline2
                    .copyWith(fontSize: 28)),
            Column(
              children: <Widget>[
                Text(
                  "12",
                  textAlign: TextAlign.center,
                  style: Theme.of(context)
                      .textTheme
                      .headline2
                      .copyWith(fontSize: 28),
                ),
                Text(
                  "ساعت",
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.headline4,
                ),
              ],
            )
          ],
        ),
        SizedBox(
          height: 7,
        ),
        Text(
          "تا قرعه کشی !",
          softWrap: true,
          overflow: TextOverflow.fade,
          maxLines: 2,
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.bodyText1,
        ),
      ],
    );
  }
}
