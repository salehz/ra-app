import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class DashboardCards extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<BoxShadow> raBoxShadows = [
      BoxShadow(
        color: Theme.of(context).colorScheme.secondary.withAlpha(60),
        offset: Offset(-3, 0),
        blurRadius: 6.0,
        spreadRadius: 0.0,
      ),
      BoxShadow(
        color: Theme.of(context).colorScheme.secondary.withAlpha(60),
        offset: Offset(0, 3),
        blurRadius: 15.0,
        spreadRadius: 0.0,
      ),
    ];
    return Column(
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        //// card 1
        Padding(
          padding: const EdgeInsets.only(bottom: 16),
          child: Stack(
            children: [
              Container(
                width: 375,
                height: 87,
                alignment: Alignment.centerRight,
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 18, horizontal: 22),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child:
                            SvgPicture.asset('assets/images/icons/ballot.svg'),
                      ),
                      SizedBox(
                        width: 16,
                      ),
                      Expanded(
                        flex: 3,
                        child: Text(
                          "در نظرسنجی شرکت کن و جایزه بگیر",
                          softWrap: true,
                          overflow: TextOverflow.fade,
                          maxLines: 2,
                          textAlign: TextAlign.right,
                          style: Theme.of(context).textTheme.bodyText1,
                        ),
                      ),
                      Expanded(flex: 1, child: SizedBox())
                    ],
                  ),
                ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(16),
                  color: Theme.of(context).colorScheme.primary,
                ),
              ),
              Positioned(
                  left: 0,
                  bottom: 0,
                  child: ClipRRect(
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(25),
                        topRight: Radius.circular(50)),
                    child: Container(
                      alignment: Alignment.center,
                      width: 33,
                      height: 32,
                      child: Icon(
                        Icons.arrow_forward,
                        size: 18,
                      ),
                      decoration: BoxDecoration(
                        boxShadow: raBoxShadows,
                        color: Theme.of(context).colorScheme.secondary,
                      ),
                    ),
                  ))
            ],
          ),
        ),
        //////// Card 2
        Stack(
          children: [
            Container(
              width: 375,
              height: 87,
              alignment: Alignment.centerRight,
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 18, horizontal: 22),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: SvgPicture.asset(
                          'assets/images/icons/gift_box_copy.svg'),
                    ),
                    SizedBox(
                      width: 16,
                    ),
                    Expanded(
                      flex: 3,
                      child: Text(
                        "این جایزه تصادفی شماست! بزن روش ببین چیه",
                        softWrap: true,
                        overflow: TextOverflow.fade,
                        maxLines: 2,
                        textAlign: TextAlign.right,
                        style: Theme.of(context).textTheme.bodyText1,
                      ),
                    ),
                    Expanded(flex: 1, child: SizedBox())
                  ],
                ),
              ),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(16),
                color: Theme.of(context).colorScheme.primary,
              ),
            ),
            Positioned(
                left: 0,
                bottom: 0,
                child: ClipRRect(
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(25),
                      topRight: Radius.circular(50)),
                  child: Container(
                    alignment: Alignment.center,
                    width: 33,
                    height: 32,
                    child: Icon(
                      Icons.arrow_forward,
                      size: 18,
                    ),
                    decoration: BoxDecoration(
                      color: Theme.of(context).colorScheme.secondary,
                      boxShadow: raBoxShadows
                    ),
                  ),
                ))
          ],
        ),
      ],
    );
  }
}

////Cliper For Card
class ArrowCliper extends CustomClipper<Path> {
  double radius = 10.0;

  @override
  Path getClip(Size size) {
    var path = Path()
      ..addRRect(RRect.fromRectAndRadius(
          Rect.fromLTWH(size.width, 0, 0 - size.width, size.height),
          Radius.circular(16)));

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}
