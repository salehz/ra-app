import 'package:flutter/material.dart';

class WinnerScrollBar extends StatelessWidget {
  const WinnerScrollBar({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: EdgeInsets.only(top: 15),
      scrollDirection: Axis.horizontal,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12),
          child: Column(
            children: <Widget>[
              CircleAvatar(
                radius: 28.0,
                backgroundImage: AssetImage('assets/images/bitmap.jpg'),
              ),
              Container(
                alignment: Alignment.center,
                child: Text(
                  '204',
                  style: Theme.of(context).textTheme.headline1,
                ),
                height: 22,
              )
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12),
          child: Column(
            children: <Widget>[
              CircleAvatar(
                radius: 28.0,
                backgroundImage: AssetImage('assets/images/bitmap.jpg'),
              ),
              Container(
                alignment: Alignment.center,
                child: Text(
                  '204',
                  style: Theme.of(context).textTheme.headline1,
                ),
                height: 22,
              )
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12),
          child: Column(
            children: <Widget>[
              CircleAvatar(
                radius: 28.0,
                backgroundImage: AssetImage('assets/images/bitmap.jpg'),
              ),
              Container(
                alignment: Alignment.center,
                child: Text(
                  '204',
                  style: Theme.of(context).textTheme.headline1,
                ),
                height: 22,
              )
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12),
          child: Column(
            children: <Widget>[
              CircleAvatar(
                radius: 28.0,
                backgroundImage: AssetImage('assets/images/bitmap.jpg'),
              ),
              Container(
                alignment: Alignment.center,
                child: Text(
                  '204',
                  style: Theme.of(context).textTheme.headline1,
                ),
                height: 22,
              )
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12),
          child: Column(
            children: <Widget>[
              CircleAvatar(
                radius: 28.0,
                backgroundImage: AssetImage('assets/images/bitmap.jpg'),
              ),
              Container(
                alignment: Alignment.center,
                child: Text(
                  '204',
                  style: Theme.of(context).textTheme.headline1,
                ),
                height: 22,
              )
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12),
          child: Column(
            children: <Widget>[
              CircleAvatar(
                radius: 28.0,
                backgroundImage: AssetImage('assets/images/bitmap.jpg'),
              ),
              Container(
                alignment: Alignment.center,
                child: Text(
                  '204',
                  style: Theme.of(context).textTheme.headline1,
                ),
                height: 22,
              )
            ],
          ),
        ),
      ],
    );
  }
}
