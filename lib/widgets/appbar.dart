import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class RAAppBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 56,
      alignment: Alignment.topCenter,
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: FractionalOffset.center,
              end: FractionalOffset.bottomCenter,
              colors: [
            Theme.of(context).backgroundColor,
            Theme.of(context).backgroundColor.withOpacity(0.0)
          ])),
      child: Padding(
        padding: const EdgeInsets.only(top: 16, left: 16, right: 16),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              width: 58,
              height: 24,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Stack(children: [
                    Icon(
                      Icons.notifications,
                      size: 24,
                    ),
                    Positioned(
                        right: 4,
                        top: 2,
                        child: CircleAvatar(
                          backgroundColor:
                              Theme.of(context).colorScheme.secondary,
                          maxRadius: 3,
                        ))
                  ]),
                  Stack(children: [
                    Icon(
                      Icons.message,
                      size: 24,
                    ),
                    Positioned(
                        right: 0,
                        top: 2,
                        child: CircleAvatar(
                          backgroundColor: Colors.red,
                          maxRadius: 3,
                        ))
                  ]),
                ],
              ),
            ),
            Container(
              width: 130,
              height: 18,
              child: SvgPicture.asset('assets/images/logo/group.svg'),
            )
          ],
        ),
      ),
    );
  }
}
