import 'package:flutter/material.dart';

class RABanner extends StatelessWidget {
  const RABanner({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 343,
      height: 120,
      alignment: Alignment.center,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 25, horizontal: 42),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Text(
              "محل قرارگیری بنر تصویری",
              softWrap: true,
              overflow: TextOverflow.fade,
              maxLines: 1,
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.headline2,
            ),
            Text(
              "لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ",
              softWrap: true,
              overflow: TextOverflow.fade,
              maxLines: 2,
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.headline3,
            ),
          ],
        ),
      ),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(16),
          color: Theme.of(context).colorScheme.secondary,
          image: DecorationImage(
              image: AssetImage(
                'assets/images/banners/street.jpg',
              ),
              fit: BoxFit.cover,
              colorFilter: ColorFilter.mode(
                  Colors.black.withOpacity(0.5), BlendMode.colorBurn))),
    );
  }
}
