import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:ra_app/widgets/appbar.dart';
import 'package:ra_app/widgets/banner.dart';
import 'package:ra_app/widgets/bottomNavigation.dart';
import 'package:ra_app/widgets/countdown.dart';
import 'package:ra_app/widgets/dashboardCards.dart';
import 'package:ra_app/widgets/massionbanner.dart';
import 'package:ra_app/widgets/missioncard.dart';
import 'package:ra_app/widgets/winnerScrollBar.dart';

class DailyDashbordFA extends StatelessWidget {
  const DailyDashbordFA({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        body: SafeArea(
          child: Stack(
            children: <Widget>[
              SizedBox(
                height: MediaQuery.of(context).size.height - 50,
                child: ListView(
                  children: <Widget>[
                    ////Profile State
                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: 180,
                      alignment: Alignment.center,
                      child: Padding(
                        padding:
                            const EdgeInsets.only(top: 56, left: 50, right: 50),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              height: 78,
                              alignment: Alignment.center,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  CircleAvatar(
                                    radius: 21.0,
                                    backgroundColor:
                                        Theme.of(context).colorScheme.primary,
                                    child: SvgPicture.asset(
                                        'assets/images/icons/star_24_px_3.svg'),
                                  ),
                                  Container(
                                    alignment: Alignment.center,
                                    child: Text(
                                      '176',
                                      style:
                                          Theme.of(context).textTheme.headline1,
                                    ),
                                    height: 22,
                                  )
                                ],
                              ),
                            ),
                            Container(
                              child: Column(
                                children: <Widget>[
                                  CircleAvatar(
                                      radius: 39.0,
                                      backgroundImage: AssetImage(
                                          'assets/images/bitmap.jpg')),
                                  Container(
                                    alignment: Alignment.center,
                                    child: Text(
                                      'حسام',
                                      style:
                                          Theme.of(context).textTheme.subtitle1,
                                    ),
                                    height: 28,
                                  )
                                ],
                              ),
                            ),
                            Container(
                              alignment: Alignment.center,
                              height: 78,
                              child: Column(
                                children: <Widget>[
                                  CircleAvatar(
                                    radius: 21.0,
                                    backgroundColor:
                                        Theme.of(context).colorScheme.primary,
                                    child: SvgPicture.asset(
                                        'assets/images/icons/emoji_events_24_px.svg'),
                                  ),
                                  Container(
                                    alignment: Alignment.center,
                                    child: Text(
                                      '80',
                                      style:
                                          Theme.of(context).textTheme.headline6,
                                    ),
                                    height: 22,
                                  )
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                    Divider(
                      color: Colors.black,
                      height: 2,
                    ),
                    //// winerss
                    Container(
                      height: 152,
                      width: MediaQuery.of(context).size.width,
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(top: 16),
                            child: SizedBox(
                              height: 25,
                              width: 96,
                              child: Text(
                                "برنده های دیشب",
                                style: Theme.of(context).textTheme.bodyText1,
                              ),
                            ),
                          ),
                          SizedBox(
                              width: MediaQuery.of(context).size.width - 10,
                              height: 111,
                              child: WinnerScrollBar())
                        ],
                      ),
                    ),
                    Divider(
                      color: Colors.black,
                      height: 2,
                    ),
                    //// Banner
                    Padding(
                        padding:
                            const EdgeInsets.only(top: 16, left: 16, right: 16),
                        child: RABanner()),
                    //// Cards
                    Padding(
                      padding:
                          const EdgeInsets.only(top: 16, left: 16, right: 16),
                      child: DashboardCards(),
                    ),
                    //// CountDown
                    Padding(
                      padding: const EdgeInsets.only(
                          bottom: 16, left: 32, right: 32, top: 32),
                      child: Container(
                        width: 185,
                        alignment: Alignment.center,
                        child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 40),
                            child: ARCountDown()),
                      ),
                    ),
                    //// Mission
                    Padding(
                        padding: const EdgeInsets.only(
                            bottom: 16, left: 16, right: 16),
                        child: RAMassionBanner()),
                    //// Divider
                    Divider(
                      color: Colors.black,
                      height: 2,
                    ),
                    //// Mision Title
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 12, bottom: 12, left: 18, right: 18),
                      child: Container(
                        width: 343,
                        alignment: Alignment.center,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              "ماموریت های شما",
                              style: Theme.of(context).textTheme.bodyText1,
                            ),
                            Text("20 اسفند 1398",
                                style: Theme.of(context).textTheme.headline4)
                          ],
                        ),
                      ),
                    ),
                    //// Divider
                    Divider(
                      color: Colors.black,
                      height: 2,
                    ),
                    //// Mission List////
                    Padding(
                      padding: const EdgeInsets.only(
                          bottom: 40, top: 16, left: 16, right: 16),
                      child: RAMissionCard(),
                    )
                  ],
                ),
              ),
              //// AppBar
              RAAppBar(),
              //// BottomNavigation
              RABottomNavigation()
            ],
          ),
        ),
      ),
    );
  }
}
